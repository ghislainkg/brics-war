#include "draw.h"

void draw_point(cairo_t * context, int x, int y) {
	x *= DOT_SIZE;
	y *= DOT_SIZE;
	cairo_rectangle(
		context,
		x - DOT_SIZE_HALF,
		y - DOT_SIZE_HALF,
		DOT_SIZE,
		DOT_SIZE);

	cairo_fill(context);
}

void draw_cube(cairo_t * context, Cube cube) {
	cairo_set_source_rgba(context, cube.color.r, cube.color.g, cube.color.b, 1);
	draw_point(context, cube.x, cube.y);
	cairo_set_source_rgba(context, 0, 0, 0, 1);
}

void draw_projectile(Projectile * projectile, cairo_t * context) {
	cairo_set_source_rgba(
		context,
		projectile->cube->color.r,
		projectile->cube->color.g,
		projectile->cube->color.b,
	1);
	draw_point(context, projectile->cube->x, projectile->cube->y);
	cairo_set_source_rgba(context, 0, 0, 0, 1);
}

void draw_robot(Robot * robot, cairo_t * context) {
	for (int i = 0; i < robot->size; ++i) {
		draw_cube(context, *robot->cubes[i]);
	}
}

static void map_aux_draw_robots(Map * map, Robot * r, void * data) {
	cairo_t * context = (cairo_t *)data;
	draw_robot(r, context);
}
static void map_aux_draw_projectiles(Map * map, Projectile * p, void * data) {
	cairo_t * context = (cairo_t *)data;
	draw_projectile(p, context);
}
static void map_draw_lines(Map * map, cairo_t * context) {
	cairo_move_to(context, DOT_SIZE * map->map_limit_left, DOT_SIZE * map->map_limit_up);

	cairo_line_to(context, DOT_SIZE * map->map_limit_left, DOT_SIZE * map->map_limit_down);
	cairo_line_to(context, DOT_SIZE * map->map_limit_right, DOT_SIZE * map->map_limit_down);
	cairo_line_to(context, DOT_SIZE * map->map_limit_right, DOT_SIZE * map->map_limit_up);
	cairo_line_to(context, DOT_SIZE * map->map_limit_left, DOT_SIZE * map->map_limit_up);

	cairo_stroke(context);
}
void draw_map(Map * map, cairo_t * context) {
	map_foreach_robot(map, map_aux_draw_robots, context);
	map_foreach_projectile(map, map_aux_draw_projectiles, context);
	map_draw_lines(map, context);
}

