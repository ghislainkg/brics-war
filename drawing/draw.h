#ifndef _DRAW_
#define _DRAW_

#include <gtk/gtk.h>

#include "../entities/robot.h"
#include "../entities/cube.h"
#include "../entities/projectile.h"
#include "../map/map.h"

#define DOT_SIZE 10
#define DOT_SIZE_HALF 5

void draw_point(cairo_t * context, int x, int y);
void draw_cube(cairo_t * context, Cube cube);
void draw_projectile(Projectile * projectile, cairo_t * context);
void draw_robot(Robot * robot, cairo_t * context);
void draw_map(Map * map, cairo_t * context);

#endif