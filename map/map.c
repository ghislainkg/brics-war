#include "map.h"

#define TRUE 1
#define FALSE 0

Map * map_create(int nbr_teams, int nbr_robots) {
	Map m;

	m.nbr_teams = nbr_teams;
	m.nbr_robots = nbr_robots;

	m.map_limit_up = MAP_LIMIT_UP;
	m.map_limit_down = MAP_LIMIT_DOWN;
	m.map_limit_left = MAP_LIMIT_LEFT;
	m.map_limit_right = MAP_LIMIT_RIGHT;
	
	m.robots = malloc(sizeof(Robot*) * m.nbr_robots);
	for(int i = 0; i < m.nbr_robots; ++i) {
		m.robots[i] = NULL;}
	m.projectiles = malloc(sizeof(Projectile*) * MAP_PROJECTILES_LIMIT);
	for (int i = 0; i < MAP_PROJECTILES_LIMIT; ++i) {
		m.projectiles[i] = NULL;}

	Map * map = malloc(sizeof(Map));
	*map = m;

	return map;
}

void map_set_limits(
	Map * map,
	int map_limit_up,
	int map_limit_down,
	int map_limit_left,
	int map_limit_right) {
	map->map_limit_up = map_limit_up;
	map->map_limit_right = map_limit_right;
	map->map_limit_left = map_limit_left;
	map->map_limit_down = map_limit_down;
}
void map_set_end(Map * map) {
	map->continu = FALSE;
}

void map_destroy(Map * map) {
	for (int i = 0; i < map->nbr_robots; ++i) {
		Robot * r = map->robots[i];
		if(r != NULL)
			free(r);
	}
	for(int i =0; i< MAP_PROJECTILES_LIMIT; i++) {
		Projectile * p = map->projectiles[i];
		if(p != NULL)
			free(p);
	}
	free(map);
}

void map_add_robot(Map * map, Robot * robot) {
	if(robot->team >= map->nbr_teams || robot->team < 0)
		return;
	for (int i = 0; i < map->nbr_robots; ++i) {
		if(map->robots[i] == NULL) {
			map->robots[i] = robot;
			return;
		}
	}
}

void map_add_projectile(Map * map, Projectile * projectile) {
	for(int i = 0; i < MAP_PROJECTILES_LIMIT; ++i) {
		if(map->projectiles[i] == NULL) {
			map->projectiles[i] = projectile;
			return;
		}
	}
}

void map_remove_robot(Map * map, Robot * robot) {
	for (int i = 0; i < map->nbr_robots; ++i) {
		if(robot == map->robots[i]) {
			map->robots[i] = NULL;
			free(robot);
			return;
		}
	}
}

void map_remove_projectile(Map * map, Projectile * projectile) {
	for (int i = 0; i < MAP_PROJECTILES_LIMIT; ++i) {
		if(projectile == map->projectiles[i]) {
			map->projectiles[i] = NULL;
			free(projectile);
			return;
		}
	}
}

void map_foreach_robot(Map * map, void (*callback)(Map * map, Robot * r, void * data), void * data) {
	for (int i = 0; i < map->nbr_robots; ++i) {
		Robot * robot = map->robots[i];
		if(robot != NULL) {
			callback(map, robot, data);
		}
	}
}

void map_foreach_projectile(Map * map, void (*callback)(Map * map, Projectile * r, void * data), void * data) {
	for (int i = 0; i < map->nbr_robots; ++i) {
		Projectile * projectile = map->projectiles[i];
		if(projectile != NULL) {
			callback(map, projectile, data);
		}
	}
}


void map_reset(Map * map) {
	map->continu = TRUE;

	for (int i = 0; i < map->nbr_robots; ++i) {
		Robot * robot = map->robots[i];
		if(robot != NULL) {
			map->robots[i] = NULL;
			free(robot);
		}
	}

	for (int i = 0; i < MAP_PROJECTILES_LIMIT; ++i) {
		Projectile * projectile = map->projectiles[i];
		if(projectile != NULL) {
			map->projectiles[i] = NULL;
			free(projectile);
		}
	}
}
