#ifndef _MAP_
#define _MAP_

#include "../entities/cube.h"
#include "../entities/projectile.h"
#include "../entities/robot.h"


#define MAP_PROJECTILES_LIMIT 1000

#define MAP_LIMIT_UP 10
#define MAP_LIMIT_DOWN 50
#define MAP_LIMIT_RIGHT 100
#define MAP_LIMIT_LEFT 10

typedef struct Map {
	Robot ** robots;
	Projectile ** projectiles;

	int nbr_teams;
	int nbr_robots;

	int continu;

	/*map properties*/
	int map_limit_up;
	int map_limit_down;
	int map_limit_left;
	int map_limit_right;
} Map;

Map * map_create(int nbr_teams, int nbr_robots);
void map_destroy(Map * map);

void map_set_limits(
	Map * map,
	int map_limit_up,
	int map_limit_down,
	int map_limit_left,
	int map_limit_right);
void map_set_end(Map * map);

void map_add_robot(Map * map, Robot * robot);
void map_add_projectile(Map * map, Projectile * projectile);
void map_remove_robot(Map * map, Robot * robot);
void map_remove_projectile(Map * map, Projectile * projectile);

void map_foreach_robot(Map * map, void (*callback)(Map * map, Robot * r, void * data), void * data);
void map_foreach_projectile(Map * map, void (*callback)(Map * map, Projectile * r, void * data), void * data);

void map_reset(Map * map);

#endif