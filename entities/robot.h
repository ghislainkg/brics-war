#ifndef _ROBOT_
#define _ROBOT_

#include "cube.h"
#include "projectile.h"

#define ROBOT_SIZE 6
#define ROBOT_SPACE_SPEED 1
#define ROBOT_TIME_SPEED 3
#define ROBOT_DEFAULT_DIRECTION UP

typedef struct Robot {
	Cube ** cubes;
	int size;

	int id;
	int team;

	// Ce qui suit est a implemente dans la map
	// Pour les robots controlle par l'ordinateur
	
	// Le nombre de tours restant avant le prochain changement de direction
	int direction_timeout;
	// Tantque le move_timeout n'est pas a 0, on ne bouge pas
	int move_timeout;

	int is_shooting;
} Robot;

Robot * robot_create(int c, int y, int id, int team, Color color);
void robot_destroy(Robot * robot);

void robot_move(Robot * robot);
void robot_set_move_timeout(Robot * robot, int timeout);
void robot_decrement_move_timeout(Robot * robot);
int robot_move_timeout_expired(Robot * robot);

void robot_change_direction(Robot * robot, Direction direction);
void robot_set_direction_timeout(Robot * robot, int timeout);
void robot_decrement_direction_timeout(Robot * robot);
int robot_direction_timeout_expired(Robot * robot);

void robot_translation(Robot * robot, int x, int y);
Projectile * shoot(Robot * robot);

#endif