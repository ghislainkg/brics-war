#include "robot.h"

static Cube ** create_robot_cubes(int x, int y, Color color) {
	Cube ** cubes = malloc(sizeof(Cube*) * ROBOT_SIZE);
	Cube * center = cube_create(x, y, color, ROBOT_DEFAULT_DIRECTION, ROBOT_SPACE_SPEED, ROBOT_TIME_SPEED);
	cubes[0] = center;
	Cube * up = cube_create(x, y-1, color, ROBOT_DEFAULT_DIRECTION, ROBOT_SPACE_SPEED, ROBOT_TIME_SPEED);
	cubes[1] = up;
	Cube * left = cube_create(x-1, y, color, ROBOT_DEFAULT_DIRECTION, ROBOT_SPACE_SPEED, ROBOT_TIME_SPEED);
	cubes[2] = left;
	Cube * right = cube_create(x+1, y, color, ROBOT_DEFAULT_DIRECTION, ROBOT_SPACE_SPEED, ROBOT_TIME_SPEED);
	cubes[3] = right;
	Cube * left_down = cube_create(x-1, y+1, color, ROBOT_DEFAULT_DIRECTION, ROBOT_SPACE_SPEED, ROBOT_TIME_SPEED);
	cubes[4] = left_down;
	Cube * right_down = cube_create(x+1, y+1, color, ROBOT_DEFAULT_DIRECTION, ROBOT_SPACE_SPEED, ROBOT_TIME_SPEED);
	cubes[5] = right_down;

	return cubes;
}

Robot * robot_create(int x, int y, int id, int team, Color color) {
	Robot b;
	b.cubes = create_robot_cubes(x, y, color);
	b.size = ROBOT_SIZE;
	b.team = team;
	b.id = id;
	b.direction_timeout = 0;
	b.move_timeout = 1;
	b.is_shooting = 0;

	Robot * robot = malloc(sizeof(Robot));
	*robot = b;

	return robot;
}

void robot_destroy(Robot * robot) {
	for (int i = 0; i < robot->size; ++i) {
		cube_destroy(robot->cubes[i]);
	}
	free(robot);
}

void robot_move(Robot * robot) {
	for (int i = 0; i < robot->size; ++i) {
		cube_move(robot->cubes[i]);
	}
}

void robot_set_move_timeout(Robot * robot, int timeout) {
	robot->move_timeout = timeout;
}
void robot_decrement_move_timeout(Robot * robot) {
	robot->move_timeout --;
}
int robot_move_timeout_expired(Robot * robot) {
	return robot->move_timeout == 0;
}


static void set_up(Robot * robot) {
	int x = robot->cubes[0]->x;
	int y = robot->cubes[0]->y;
	
	cube_change_pos(robot->cubes[1], x, y-1);
	cube_change_pos(robot->cubes[2], x-1, y);
	cube_change_pos(robot->cubes[3], x+1, y);
	cube_change_pos(robot->cubes[4], x-1, y+1);
	cube_change_pos(robot->cubes[5], x+1, y+1);
}

static void set_down(Robot * robot) {
	int x = robot->cubes[0]->x;
	int y = robot->cubes[0]->y;
	
	cube_change_pos(robot->cubes[1], x, y+1);
	cube_change_pos(robot->cubes[2], x+1, y);
	cube_change_pos(robot->cubes[3], x-1, y);
	cube_change_pos(robot->cubes[4], x+1, y-1);
	cube_change_pos(robot->cubes[5], x-1, y-1);
}

static void set_left(Robot * robot) {
	int x = robot->cubes[0]->x;
	int y = robot->cubes[0]->y;
	
	cube_change_pos(robot->cubes[1], x-1, y);
	cube_change_pos(robot->cubes[2], x, y+1);
	cube_change_pos(robot->cubes[3], x, y-1);
	cube_change_pos(robot->cubes[4], x+1, y+1);
	cube_change_pos(robot->cubes[5], x+1, y-1);
}

static void set_right(Robot * robot) {
	int x = robot->cubes[0]->x;
	int y = robot->cubes[0]->y;
	
	cube_change_pos(robot->cubes[1], x+1, y);
	cube_change_pos(robot->cubes[2], x, y-1);
	cube_change_pos(robot->cubes[3], x, y+1);
	cube_change_pos(robot->cubes[4], x-1, y-1);
	cube_change_pos(robot->cubes[5], x-1, y+1);
}


void robot_change_direction(Robot * robot, Direction direction) {
	for (int i = 0; i < robot->size; ++i) {
		cube_change_direction(robot->cubes[i], direction);
	}
	switch(direction) {
		case RIGHT:
			set_right(robot);
			break;
		case LEFT:
			set_left(robot);
			break;
		case UP:
			set_up(robot);
			break;
		case DOWN:
			set_down(robot);
			break;
	}
}

void robot_set_direction_timeout(Robot * robot, int timeout) {
	robot->direction_timeout = timeout;
}

void robot_decrement_direction_timeout(Robot * robot) {
	robot->direction_timeout --;
}

int robot_direction_timeout_expired(Robot * robot) {
	return robot->direction_timeout == 0;
}

void robot_translation(Robot * robot, int x, int y) {
	for (int i = 0; i < robot->size; ++i) {
		cube_change_pos_relative(robot->cubes[i], x, y);
	}
}

Projectile * shoot(Robot * robot) {
	return projectile_create(robot->cubes[1]->x, robot->cubes[1]->y, robot->cubes[1]->direction, robot->team);
}