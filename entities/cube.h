#ifndef _CUBE_
#define _CUBE_

#include <stdlib.h>

typedef struct Color {
	double r;
	double g;
	double b;
} Color;

typedef enum Direction {
	RIGHT,
	LEFT,
	UP,
	DOWN
} Direction;

typedef struct Cube {
	int x;
	int y;
	Color color;

	Direction direction;
	int space_speed;

	// the cube move only when the timeout reach the value of time_speed
	// So the more time_speed is high the lower the cube will be
	int time_speed;
	int timeout;
} Cube;

Cube * cube_create(int x, int y, Color color, Direction direction, int space_speed, int time_speed);
void cube_destroy(Cube * cube);
void cube_move(Cube * cube);
void cube_change_direction(Cube * cube, Direction direction);
void cube_change_pos(Cube * c, int x, int y);
void cube_change_pos_relative(Cube * c, int x, int y);

#endif