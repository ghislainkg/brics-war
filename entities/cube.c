#include "cube.h"

Cube * cube_create(int x, int y, Color color, Direction direction, int space_speed, int time_speed) {
	Cube b;
	b.x = x;
	b.y = y;
	b.color = color;
	b.direction = direction;
	b.space_speed = space_speed;
	b.time_speed = time_speed;
	b.timeout = 0;

	Cube * cube = malloc(sizeof(Cube));
	*cube = b;

	return cube;
}

void cube_destroy(Cube * cube) {
	free(cube);
}

void cube_move(Cube * cube) {
	if(cube->timeout < cube->time_speed) {
		cube->timeout++;
		return;
	}
	else {
		cube->timeout = 0;
	}
	switch(cube->direction) {
		case RIGHT:
			cube->x += cube->space_speed;
			break;
		case LEFT:
			cube->x -= cube->space_speed;
			break;
		case UP:
			cube->y -= cube->space_speed;
			break;
		case DOWN:
			cube->y += cube->space_speed;
			break;
	}
}

void cube_change_direction(Cube * cube, Direction direction) {
	cube->direction = direction;
}

void cube_change_pos(Cube * c, int x, int y) {
	c->x = x;
	c->y = y;
}

void cube_change_pos_relative(Cube * c, int x, int y) {
	c->x += x;
	c->y += y;
}

