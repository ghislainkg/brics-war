#include "projectile.h"

Projectile * projectile_create(int x, int y, Direction direction, int team) {

	Projectile aux;
	Color color = {0.9, 0.2, 0.2};
	aux.cube = cube_create(x, y, color, direction, PROJECTILE_SPACE_SPEED, PROJECTILE_TIME_SPEED);
	aux.team = team;

	Projectile * p = malloc(sizeof(Projectile));
	*p = aux;

	return p;
}

void projectile_destroy(Projectile * projectile) {
	free(projectile->cube);
	free(projectile);
}

void projectile_move(Projectile * projectile) {
	cube_move(projectile->cube);
}
