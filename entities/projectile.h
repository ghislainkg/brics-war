#ifndef _PROJECTILE_
#define _PROJECTILE_

#include "cube.h"

#define PROJECTILE_SPACE_SPEED 1
#define PROJECTILE_TIME_SPEED 0

typedef struct Projectile {
	Cube * cube;
	int team;
} Projectile;

Projectile * projectile_create(int x, int y, Direction direction, int team);
void projectile_destroy(Projectile * projectile);
void projectile_move(Projectile * projectile);

#endif