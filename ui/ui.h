#ifndef _UI_
#define _UI_

#include <gtk/gtk.h>

typedef struct UI {
	GtkWidget * window;
	GtkWidget * drawarea;
	GtkWidget * btnQuit;
	GtkWidget * btnStopStart;
	GtkWidget * info_label;
} UI;


#define ID_BUTTON_RESTART 1
#define ID_BUTTON_QUIT 0

int display_fin_dialog(const char* message);

#endif 