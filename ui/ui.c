#include "ui.h"

int display_fin_dialog(const char* message) {
	GtkBuilder * builder = gtk_builder_new_from_file("./uixml/ui.glade");

	GtkWidget * dialog = GTK_WIDGET(gtk_builder_get_object(builder, "dialog_fin"));
	GtkWidget * label = GTK_WIDGET(gtk_builder_get_object(builder, "dialog_fin_label"));

	gtk_label_set_text(GTK_LABEL(label), message);

	gtk_widget_show_all(dialog);
	int code = gtk_dialog_run(GTK_DIALOG(dialog));
	gtk_widget_destroy(GTK_WIDGET(dialog));
	return code;
}