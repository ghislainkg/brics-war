#include "partie.h"

static void on_draw(GtkWidget * drawarea, cairo_t * context, void * data) {
	Map * map = (Map *) data;
	draw_map(map, context);

	int width = gtk_widget_get_allocated_width(drawarea);
	int height = gtk_widget_get_allocated_height(drawarea);
	gtk_widget_queue_draw_area(drawarea, 0, 0, width, height);
}

void start_partie(int nbrRobots, UI ui) {
	/*THA MAP*/
	Map * map = get_first_map(nbrRobots);

	// DRAW SIGNAL
	g_signal_connect(ui->drawarea, "draw", G_CALLBACK(on_draw), map);
	// MAP THREAD
	start_play_thread(map);
	// INFO THREAD
	start_info_thread(&ui, map);
}

void end_partie() {

}
