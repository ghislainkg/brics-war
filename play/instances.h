#ifndef _INSTANCES_
#define _INSTANCES_

#include "../map/map.h"

void fill_map_random(Map * map);
Map * get_first_map(int nbr_robots);

#endif
