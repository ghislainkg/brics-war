#ifndef _GUIDAGE_
#define _GUIDAGE_

#include <gtk/gtk.h>

#include "../map/map.h"
#include "../entities/robot.h"
#include "instances.h"

void on_key_press(GtkWidget *widget, GdkEvent *e, gpointer data);
void on_release_press(GtkWidget *widget, GdkEvent *e, gpointer data);

void on_quit_button_clicked(GtkWidget * widget, gpointer data);
void on_start_button_clicked(GtkWidget * widget, gpointer data);

#endif