/**/
#include "instances.h"

static int get_random_valid_position_x(const Map * map) {
	int x = 0;
	while(x == 0) {
		x = rand() % map->map_limit_right + map->map_limit_left;
		for (int i = 0; i < map->nbr_robots; ++i) {
			if(map->robots[i] != NULL) {
				if(map->robots[i]->cubes[0]->x <= x+1 && map->robots[i]->cubes[0]->x <= x-1)
					break;
			}
		}
	}
	return x;
}
static int get_random_valid_position_y(const Map * map) {
	int y = 0;
	while(y == 0) {
		y = rand() % map->map_limit_down + map->map_limit_up;
		for (int i = 0; i < map->nbr_robots; ++i) {
			if(map->robots[i] != NULL) {
				if(map->robots[i]->cubes[0]->y <= y+1 && map->robots[i]->cubes[0]->y <= y-1)
					break;
			}
		}
	}
	return y;
}

void fill_map_random(Map * map) {
	int x = get_random_valid_position_x(map);
	int y = get_random_valid_position_y(map);
	Color color = {1, 0, 1};
	Robot * joueur = robot_create(x, y, 0/*id*/, 0/*team*/, color);
	map_add_robot(map, joueur);

	for (int i = 1; i < map->nbr_robots; ++i) {
		x = get_random_valid_position_x(map);
		y = get_random_valid_position_y(map);
		Robot * robot;
		if(i < 3) {
			Color c = {0, i, 0};
			robot = robot_create(x, y, i, 0, c);
		}
		else {
			Color c = {0, 0, 1};
			robot = robot_create(x, y, i, 1, c);
		}
		map_add_robot(map, robot);
	}
}

Map * get_first_map(int nbr_robots) {
	Map * map = map_create(2, nbr_robots);
	fill_map_random(map);
	map_set_limits(map, 5, 50, 5, 75);
	return map;
}

