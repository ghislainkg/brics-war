#include "guidage.h"


void on_key_press(GtkWidget *widget, GdkEvent *e, gpointer data) {
	Map * map = (Map *) data;
	Robot * robot = map->robots[0];
	GdkEventKey * ev = (GdkEventKey *) e;

	if(robot == NULL)
		return;

	switch(ev->keyval) {
		case GDK_KEY_Left:
			robot_change_direction(robot, LEFT);
			robot_set_move_timeout(robot, 0); // Le robot peux se deplacer
			break;
		case GDK_KEY_Up:
			robot_change_direction(robot, UP);
			robot_set_move_timeout(robot, 0); // Le robot peux se deplacer
			break;
		case GDK_KEY_Right:
			robot_change_direction(robot, RIGHT);
			robot_set_move_timeout(robot, 0); // Le robot peux se deplacer
			break;
		case GDK_KEY_Down:
			robot_change_direction(robot, DOWN);
			robot_set_move_timeout(robot, 0); // Le robot peux se deplacer
			break;
		case GDK_KEY_space:
			robot->is_shooting = 1;
			break;
	}
}

void on_release_press(GtkWidget *widget, GdkEvent *e, gpointer data) {
	Map * map = (Map *) data;
	Robot * robot = map->robots[0];
	GdkEventKey * ev = (GdkEventKey *) e;

	if(robot == NULL)
		return;

	if(ev->keyval == GDK_KEY_space) {
		robot->is_shooting = 0;
	}
	else {
		robot_set_move_timeout(robot, 10); // Le robot ne peut plus se deplacer
	}
}


void on_quit_button_clicked(GtkWidget * widget, gpointer data) {
	Map * map = (Map*) data;
	map_destroy(map);
	gtk_main_quit();
}

void on_start_button_clicked(GtkWidget * widget, gpointer data) {
	Map * map = (Map * )data;
	map_reset(map);
	fill_map_random(map);
}
