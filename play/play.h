#ifndef _PLAY_
#define _PLAY_

#include <stdio.h>
#include <time.h>
#include <string.h>

#include <gtk/gtk.h>

#include "../map/map.h"
#include "../entities/robot.h"
#include "../entities/projectile.h"

void start_play_thread(Map * map);

#endif