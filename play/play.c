#include "play.h"


/***********************************NETTOYAGE DU CHAMP DE BATTAILLE*****************************************/
static void check_robot_vs_projectile(Map * map, Robot * robot, void * data) {
	Projectile * projectile = (Projectile *) data;
	if(projectile->team != robot->team && projectile->cube->x == robot->cubes[0]->x && projectile->cube->y == robot->cubes[0]->y) {
		map_remove_robot(map, robot);
		map_remove_projectile(map, projectile);
	}
}
static void check_projectile_touch(Map * map, Projectile * projectile, void * data) {
	map_foreach_robot(map, check_robot_vs_projectile, projectile);
}
/*Enleve de la map les robots qui sont a la meme position q'un projectile*/
static void check_dead(Map * map) {
	map_foreach_projectile(map, check_projectile_touch, NULL);
}


/***********************************DEPLACEMENT DES PROJECTILES*****************************************/
/*Verifie si un projectile a atteint les limites d'une map*/
static int check_projectile_limit(Map * map, Projectile * projectile) {
	if(
		projectile->cube->x < map->map_limit_left || 
		projectile->cube->x > map->map_limit_right ||
		projectile->cube->y < map->map_limit_up ||
		projectile->cube->y > map->map_limit_down
	) {
		return TRUE;
	}

	return FALSE;
}
/*On deplace un projectile*/
static void aux_move_projectile(Map * map, Projectile * projectile, void * data) {
	// On verifie si le projectile a atteint les limites
	if(check_projectile_limit(map, projectile)) {
		// Si oui, on l'enleve de la map
		map_remove_projectile(map, projectile);
	}else {
		// Si non, on le deplace
		projectile_move(projectile);
	}
}
/***********************************LES DEPLACEMENTS DES ROBOTS*****************************************/
/*Change aleatoirement la direction d'un robot*/
static void change_robot_direction_random(Robot * robot) {
	int direction = rand() % 4;
	switch(direction) {
		case 0:
			robot_change_direction(robot, RIGHT);
			break;
		case 1:
			robot_change_direction(robot, LEFT);
			break;
		case 2: 
			robot_change_direction(robot, UP);
			break;
		case 3:
			robot_change_direction(robot, DOWN);
			break;
	}
}
/*prise de decision aleatoire des robots non joueurs*/
static void robot_decision(Map * map, Robot * robot, void * data) {
	if(robot == map->robots[0])
		return;

	// DECISION tirer ou pas 
	int will_shoot = rand() % 5;
	if(will_shoot == 1) {
		map_add_projectile(map, shoot(robot));
	}

	// DECISION ou vas-t-on
	if(robot_direction_timeout_expired(robot)) {
		// On peut se deplacer, on choisi un chemin
		change_robot_direction_random(robot);
		// On suis cette direction pendant combien de tour ?
		int timeout = rand() % 100;
		robot_set_direction_timeout(robot, timeout);
	}
	else {
		// On ne peut pas changer de direction, on laisse passer ce tour
		robot_decrement_direction_timeout(robot);
	}

	// DECISION on s'arrete on pas ?
	if(robot_move_timeout_expired(robot)) {
		// On peut se deplacer on s'arrete ou pas ?
		int decision = rand() % 2;
		if(decision == 0) {
			// On ne se deplace pas
			// Pendant combien de tour
			int timeout = rand() % 10;
			robot_set_move_timeout(robot, timeout);
		}
	}
	else {
		// On attends
		robot_decrement_move_timeout(robot);
	}
}

/*Verifi si un robot a atteint les limite d'une map, et le place du cote
oppose si oui*/
static int check_robot_limit(Map * map, Robot * robot) {
	if(robot->cubes[0]->x < map->map_limit_left) {
		robot_translation(robot, map->map_limit_right - map->map_limit_left, 0);
		return TRUE;
	}
	else if(robot->cubes[0]->x > map->map_limit_right) {
		robot_translation(robot, map->map_limit_left - map->map_limit_right, 0);
		return TRUE;
	}
	else if(robot->cubes[0]->y < map->map_limit_up) {
		robot_translation(robot, 0, map->map_limit_down - map->map_limit_up);
		return TRUE;
	}
	else if(robot->cubes[0]->y > map->map_limit_down) {
		robot_translation(robot, 0, map->map_limit_up - map->map_limit_down);
		return TRUE;
	}

	return FALSE;
}
// On deplace un robot
static void aux_robot_move(Map * map, Robot * robot, void * data) {
	// On verifie s'il le robot a atteint les limites du terrain
	check_robot_limit(map, robot);
	// On verifi si le robot peut se deplacer
	if(robot_move_timeout_expired(robot)) {
		// Si oui, on le deplace
		robot_move(robot);
	}
}

static void robots_shoot(Map * map, Robot * robot, void * data) {
	if(robot != map->robots[0])
		return;
	if(robot->is_shooting) {
		map_add_projectile(map, shoot(robot));
	}
}


/***********************************ON FAIT TOURNER LA BOUCLE*****************************************/
static void play_round(Map * map) {
	check_dead(map);
	map_foreach_robot(map, robots_shoot, NULL);
	map_foreach_robot(map, robot_decision, NULL);
	map_foreach_robot(map, aux_robot_move, NULL);
	map_foreach_projectile(map, aux_move_projectile, NULL);
}

gboolean map_thread_execution(gpointer data) {
	Map * map = (Map*)data;
	play_round(map);

	return G_SOURCE_CONTINUE;
}
static gpointer map_thread(gpointer data) {
	g_timeout_add(10, map_thread_execution, data);
	return NULL;
}
void start_play_thread(Map * map) {
	g_thread_new("war_map_thread", map_thread, map);
}