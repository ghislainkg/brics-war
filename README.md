# Brics War

![alt text](brics.png "Snake")

# Build and Run

Make sure to have GTK+3 installed:
```bash
sudo apt-get install build-essential libgtk-3-dev
```

Then in the project directory:
```bash
mkdir obj
make
```

Run:
```bash
./war
```
