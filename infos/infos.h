#ifndef _INFO_
#define _INFO_

#include <stdlib.h>
#include <string.h>
#include <gtk/gtk.h>

#include "../map/map.h"
#include "../ui/ui.h"
#include "../play/instances.h"

gboolean info_thread_execution(gpointer data);

gpointer info_thread(gpointer data);

void start_info_thread(UI * ui, Map * map);

#endif