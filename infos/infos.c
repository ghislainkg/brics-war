#include "infos.h"

void on_restart(Map * map) {
	map_reset(map);
	fill_map_random(map);
}

void on_quit(Map * map) {
	map_destroy(map);
	gtk_main_quit();
}

static void call_back_info_thread_execution
	(Map * map, Robot * r, void * data) {
	void ** arg = (void ** ) data;
	int * count_robot_teams = (int *) arg[0];
	GString * message = (GString *) arg[1];

	count_robot_teams[ r->team ] ++;

	g_string_append_printf(message, "Team %d Robot %d Vivant\n", r->team, r->id);
}
gboolean info_thread_execution(void * data) {
	UI * ui = (UI * ) ((void **) data)[0];
	Map * map = (Map *) ((void **) data)[1];

	GtkWidget * info_label = (GtkWidget * ) ui->info_label;

	GString * message = g_string_new("");

	int * count_robot_teams = malloc(sizeof(int)*map->nbr_teams);
	memset(count_robot_teams, 0, sizeof(int)*map->nbr_teams);
	void * arg[] = {count_robot_teams, message};
	map_foreach_robot(map, call_back_info_thread_execution, arg);

	if(count_robot_teams[0] == 0) {
		// Les joueurs sont tous mort, fin de la partie
		gtk_label_set_text(GTK_LABEL(info_label), "Fin de la partie, vous avez perdu");
		int code = display_fin_dialog("Fin de la partie, vous avez perdu");
		if(code == ID_BUTTON_RESTART) {
			on_restart(map);
		}
		else if (code == ID_BUTTON_QUIT) {
			on_quit(map);
			return FALSE;
		}
	}
	else {
		int i;
		for (i = 1; i < map->nbr_teams; ++i) {
			if(count_robot_teams[i] <= 0)
				break;
		}
		if(i < map->nbr_teams) {
			// Les autres equipes sont toutes mortes, Fin de la partie
			gtk_label_set_text(GTK_LABEL(info_label), "Fin de la partie, vous avez gagne");
			int code = display_fin_dialog("Fin de la partie, vous avez gagne");
			if(code == ID_BUTTON_RESTART) {
				on_restart(map);
			}
			else if (code == ID_BUTTON_QUIT) {
				on_quit(map);
				return FALSE;
			}
		}
		else {
			// La partie continue
			gtk_label_set_text(GTK_LABEL(info_label), "");
			gtk_label_set_text(GTK_LABEL(info_label), message->str);
		}
	}

	free(count_robot_teams);
	return TRUE;
}
gpointer info_thread(gpointer data) {
	g_timeout_add(50, info_thread_execution, data);
	return NULL;
}

void start_info_thread(UI * ui, Map * map) {
	void ** arg = malloc(sizeof(void*)*2);
	arg[0] = ui;
	arg[1] = map;
	g_thread_new("war_info_thread", info_thread, (void*)arg);
}
