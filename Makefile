obj=obj/
entities=entities/
map=map/
utils=utils/
drawing=drawing/
support=main_support/

opt= -Wall -g

targets= $(obj)ui.o $(obj)instances.o $(obj)guidage.o $(obj)play.o $(obj)infos.o $(obj)cube.o $(obj)projectile.o $(obj)robot.o $(obj)map.o $(obj)draw.o $(obj)main.o

all: war

war: $(targets)
	gcc $(opt) $(targets) -o war `pkg-config --cflags gtk+-3.0` `pkg-config --libs gtk+-3.0`

# entities
$(obj)cube.o: entities/cube.c entities/cube.h
	gcc -c $(opt) entities/cube.c -o $(obj)cube.o
$(obj)projectile.o: entities/projectile.c entities/projectile.h
	gcc -c $(opt) entities/projectile.c -o $(obj)projectile.o
$(obj)robot.o: entities/robot.c entities/robot.h
	gcc -c $(opt) entities/robot.c -o $(obj)robot.o

# map
$(obj)map.o: map/map.c map/map.h
	gcc -c $(opt) map/map.c -o $(obj)map.o `pkg-config --cflags gtk+-3.0` `pkg-config --libs gtk+-3.0`

# drawing
$(obj)draw.o: drawing/draw.c drawing/draw.h
	gcc -c $(opt) drawing/draw.c -o $(obj)draw.o `pkg-config --cflags gtk+-3.0` `pkg-config --libs gtk+-3.0`

# play
$(obj)play.o: play/play.c play/play.h
	gcc -c $(opt) play/play.c -o $(obj)play.o `pkg-config --cflags gtk+-3.0` `pkg-config --libs gtk+-3.0`
$(obj)guidage.o: play/guidage.c play/guidage.h
	gcc -c $(opt) play/guidage.c -o $(obj)guidage.o `pkg-config --cflags gtk+-3.0` `pkg-config --libs gtk+-3.0`
$(obj)instances.o: play/instances.c play/instances.h
	gcc -c $(opt) play/instances.c -o $(obj)instances.o `pkg-config --cflags gtk+-3.0` `pkg-config --libs gtk+-3.0`

# ui
$(obj)ui.o: ui/ui.c ui/ui.h
	gcc -c $(opt) ui/ui.c -o $(obj)ui.o `pkg-config --cflags gtk+-3.0` `pkg-config --libs gtk+-3.0`

# infos
$(obj)infos.o: infos/infos.c infos/infos.h
	gcc -c $(opt) infos/infos.c -o $(obj)infos.o `pkg-config --cflags gtk+-3.0` `pkg-config --libs gtk+-3.0`

# main
$(obj)main.o: main.c
	gcc -c $(opt) main.c -o $(obj)main.o `pkg-config --cflags gtk+-3.0` `pkg-config --libs gtk+-3.0`

clean:
	rm -rf $(obj)* war