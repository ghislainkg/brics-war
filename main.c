#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <gtk/gtk.h>

#include "map/map.h"
#include "infos/infos.h"
#include "play/play.h"
#include "play/guidage.h"
#include "play/instances.h"
#include "ui/ui.h"
#include "drawing/draw.h"

void on_draw(GtkWidget * drawarea, cairo_t * context, void * data) {
	Map * map = (Map *) data;
	draw_map(map, context);

	int width = gtk_widget_get_allocated_width(drawarea);
	int height = gtk_widget_get_allocated_height(drawarea);
	gtk_widget_queue_draw_area(drawarea, 0, 0, width, height);
}

#define CHECK(pointer) \
	if(pointer == NULL) \
		exit(1);

void create_window(GtkApplication * app, gpointer data) {
	GtkWidget * window;
	GtkWidget * drawarea;
	GtkWidget * btnQuit;
	GtkWidget * btnStopStart;
	GtkWidget * info_label;

	GtkBuilder * builder = gtk_builder_new_from_file("uixml/ui.glade");
	CHECK(builder)

	window = GTK_WIDGET(gtk_builder_get_object(builder, "window"));
	CHECK(window)
	drawarea = GTK_WIDGET(gtk_builder_get_object(builder, "drawarea"));
	CHECK(drawarea)
	btnQuit = GTK_WIDGET(gtk_builder_get_object(builder, "btn_quit"));
	CHECK(btnQuit)
	btnStopStart = GTK_WIDGET(gtk_builder_get_object(builder, "btn_stop_start"));
	CHECK(btnStopStart)
	info_label = GTK_WIDGET(gtk_builder_get_object(builder, "info"));
	CHECK(info_label)

	UI ui;
	ui.window = window;
	ui.drawarea = drawarea;
	ui.btnQuit = btnQuit;
	ui.btnStopStart = btnStopStart;
	ui.info_label = info_label;


	/*THA MAP*/
	Map * map = get_first_map(10);

	// DRAW SIGNAL
	g_signal_connect(drawarea, "draw", G_CALLBACK(on_draw), map);
	// MAP THREAD
	start_play_thread(map);
	// INFO THREAD
	start_info_thread(&ui, map);


	// KEYBOARD EVENTS
	gtk_widget_add_events(window, GDK_KEY_PRESS_MASK | GDK_KEY_RELEASE_MASK);
	g_signal_connect(window, "key-press-event", G_CALLBACK(on_key_press), map);
	g_signal_connect(window, "key-release-event", G_CALLBACK(on_release_press), map);
	g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), NULL);

	// BUTTON EVENTS
	g_signal_connect(btnQuit, "clicked", G_CALLBACK(on_quit_button_clicked), map);
	g_signal_connect(btnStopStart, "clicked", G_CALLBACK(on_start_button_clicked), map);

	gtk_widget_show_all(window);
	gtk_main();
}

int main(int argc, char *argv[]) {
	srand(time(NULL));

	GtkApplication * app;
	app = gtk_application_new("war.war", G_APPLICATION_FLAGS_NONE);

	g_signal_connect(app, "activate", G_CALLBACK(create_window), NULL);
	int status = g_application_run(G_APPLICATION(app), argc, argv);

	g_object_unref(app);

	return status;
}