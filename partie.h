#ifndef _PARTIE_
#define _PARTIE_

#include "map/map.h"
#include "infos/infos.h"
#include "play/play.h"
#include "play/instances.h"
#include "drawing/draw.h"
#include "ui/ui.h"

void start_partie();

#endif